package main

import (
	"bufio"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

func (s *server) indexHandler() http.HandlerFunc {
	type Article struct {
		ID          int    `json:"id"`
		Title       string `json:"title"`
		Description string `json:"description"`
		Content     string `json:"content"`
	}

	type Data struct {
		Articles []Article
	}

	return func(w http.ResponseWriter, r *http.Request) {
		// http.ServeFile(w, r, "public")
		resp, err := http.Get("http://localhost:8081/api/articles")
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}
		defer resp.Body.Close()

		buffer := bufio.NewReader(resp.Body)

		body, err := ioutil.ReadAll(buffer)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}

		var data Data

		json.Unmarshal(body, &data.Articles)

		s.tmpl.Execute(w, data)
	}
}
