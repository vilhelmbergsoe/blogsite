package main

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"gorm.io/gorm"
)

func (s *server) getArticlesHandler() http.HandlerFunc {
	type Article struct {
		ID          int    `json:"id"`
		Title       string `json:"title"`
		Description string `json:"description"`
		Content     string `json:"content"`
	}

	type Articles []Article

	return func(w http.ResponseWriter, r *http.Request) {
		var articles Articles

		if err := s.db.Select("id", "title", "description", "content").Find(&articles).Error; err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}

		json.NewEncoder(w).Encode(articles)
	}
}

func (s *server) getArticleHandler() http.HandlerFunc {
	type Article struct {
		ID          int
		Title       string
		Description string
		Content     string
	}

	return func(w http.ResponseWriter, r *http.Request) {
		p := chi.URLParam(r, "id")
		if p == "" {
			w.WriteHeader(http.StatusBadRequest)
		}

		id, err := strconv.Atoi(p)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}

		article := Article{ID: id}

		if err = s.db.First(&article).Error; err != nil {
			w.WriteHeader(http.StatusNotFound)
		}

		json.NewEncoder(w).Encode(article)
	}
}

func (s *server) newArticleHandler() http.HandlerFunc {
	type Article struct {
		gorm.Model
		Title       string `json:"title"`
		Description string `json:"description"`
		Content     string `json:"content"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		var article Article

		err := json.NewDecoder(r.Body).Decode(&article)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
		}

		if err = s.db.Select("title", "description", "content").Create(&article).Error; err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}
	}
}

func (s *server) deleteArticleHandler() http.HandlerFunc {
	type Article struct {
		ID int
	}

	return func(w http.ResponseWriter, r *http.Request) {
		p := chi.URLParam(r, "id")
		if p == "" {
			w.WriteHeader(http.StatusBadRequest)
		}

		id, err := strconv.Atoi(p)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}

		article := Article{ID: id}

		s.db.Delete(&article)
	}
}

func (s *server) updateArticleHandler() http.HandlerFunc {
	type Article struct {
		ID          int
		Title       string `json:"title"`
		Description string `json:"description"`
		Content     string `json:"content"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		p := chi.URLParam(r, "id")
		if p == "" {
			w.WriteHeader(http.StatusBadRequest)
		}
		id, err := strconv.Atoi(p)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}

		article := Article{ID: id}

		if err = s.db.First(&article).Error; err != nil {
			w.WriteHeader(http.StatusNotFound)
		}

		var newArticle Article

		err = json.NewDecoder(r.Body).Decode(&newArticle)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
		}

		article.Title = newArticle.Title
		article.Description = newArticle.Description
		article.Content = newArticle.Content

		s.db.Save(&article)
	}
}
