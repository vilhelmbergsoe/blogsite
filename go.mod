module gitlab.com/vilhelmbergsoe/blogsite

go 1.17

require (
	github.com/go-chi/chi v1.5.4
	gorm.io/driver/sqlite v1.1.5
	gorm.io/gorm v1.21.15
)

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.2 // indirect
	github.com/mattn/go-sqlite3 v1.14.8 // indirect
)
