package main

import (
	"html/template"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

type server struct {
	router chi.Router
	db     *gorm.DB
	tmpl   *template.Template
}

type Article struct {
	gorm.Model
	Title       string
	Description string
	Content     string
}

func newServer() (*server, error) {
	r := chi.NewRouter()

	r.Use(middleware.Logger)

	db, err := gorm.Open(sqlite.Open("database.db"), &gorm.Config{})
	if err != nil {
		return nil, err
	}

	db.AutoMigrate(&Article{})

	tmpl, err := template.ParseFiles("public/index.html")
	if err != nil {
		return nil, err
	}

	s := &server{router: r, db: db, tmpl: tmpl}
	s.routes()
	return s, nil
}

func (s *server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}
