package main

import (
	"net/http"

	"github.com/go-chi/chi"
)

func (s *server) routes() {
	s.router.Get("/", s.indexHandler())

	s.router.Route("/api", func(r chi.Router) {
		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			w.Write([]byte("api working :)"))
		})

		r.Route("/articles", func(r chi.Router) {
			r.Get("/", s.getArticlesHandler())
			r.Post("/", s.newArticleHandler())

			r.Route("/{id}", func(r chi.Router) {
				r.Get("/", s.getArticleHandler())
				r.Put("/", s.updateArticleHandler())
				r.Delete("/", s.deleteArticleHandler())
			})
		})
	})
}
